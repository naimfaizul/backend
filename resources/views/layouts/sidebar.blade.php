<aside class="main-sidebar sidebar-dark-primary elevation-4">
<a href={{ Auth()->user()->name}} class="brand-link">
        <img src={{ asset('img/logotabunghaji.png') }} alt="TabungHaji Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">Hajj and Umrah </span>
    </a>
    <div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src={{ asset('img/logotabunghaji.png') }} class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth()->user()->name }}</a>
            </div>
        </div>
        <nav class="mt-2">

                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item has-treeview menu">
                        <a href="#" class="nav-link {{ request()->is('role*') ? 'active' : '' }}">
                            <i class="fas fa-user-circle nav-icon"></i>
                            <p>
                                Register User
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-secondary rounded-lg">
                            <li class="nav-item">
                                <a href={}
                                    class="">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>All User</p>
                                </a>
                                <a href={}
                                    class="nav-link {{ request()->is('role/create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>

                                    <p>Create User</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item has-treeview menu">
                        <a href="#" class="nav-link {{ request()->is('role*') ? 'active' : '' }}">
                            <i class="fas fa-user-circle nav-icon"></i>
                            <p>
                                Attendance
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-secondary rounded-lg">
                            <li class="nav-item">
                                <a href={}
                                    class="">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>All Attendance</p>
                                </a>
                                <a href={}
                                    class="nav-link {{ request()->is('role/create') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>

                                    <p>Create Attendance</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item has-treeview menu-{{ request()->is('forum*') ? 'open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('forum*') ? 'active' : '' }}">

                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Forum
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-secondary rounded-lg">
                            <li class="nav-item">
                                <a href={}
                                    class="nav-link {{ request()->is('forum/index') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>All Forum</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item has-treeview menu-{{ request()->is('user*') ? 'open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('user*') ? 'active' : '' }}">

                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Quiz
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-secondary rounded-lg">
                            <li class="nav-item">
                                <a href={}
                                    class="nav-link {{ request()->is('user/index') ? 'active' : '' }}">
                                    <i class="fas fa-users nav-icon"></i>
                                    <p>All Quiz</p>
                                </a>
                                    <a href={}
                                        class="nav-link {{ request()->is('user/create') ? 'active' : '' }}">
                                        <i class="fas fa-user-plus nav-icon"></i>
                                        <p>Create Quiz</p>
                                    </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                 <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item has-treeview menu-{{ request()->is('booking*') ? 'open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('booking*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-briefcase"></i>
                            <p>
                                Tutorial
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-secondary rounded-lg">
                            <li class="nav-item">
                                <a href={}
                                    class="nav-link {{ request()->is('booking/index') ? 'active' : '' }}">
                                    <i class=" fa fa-calendar-o nav-icon"></i>
                                    <p>All Tutorial</p>
                                </a>
                                    <a href={}
                                        class="nav-link {{ request()->is('booking/create') ? 'active' : '' }}">
                                        <i class="fas fa-calendar-plus nav-icon"></i>

                                        <p>Create Tutorial</p>
                            </li>
                        </ul>
                    </li>
                </ul>
        </nav>
    </div>
</aside>
